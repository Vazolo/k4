/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction lf2 = new Lfraction(5,-3);
      Lfraction lf1 = new Lfraction(3,-5);
      System.out.println(lf2.hashCode());
      System.out.println(lf1.hashCode());
   }

   private long num;
   private long deNom;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b < 0){
         b = b * (-1);
         a = a * (-1);
      }
      if (b == 0){
         throw new RuntimeException((String.format("Wrong input (%s, %s). Denominator cannot be 0!", a, b)));

      }
      long gcd = findGCD(a, b);

      num = a / gcd;
      deNom = b / gcd;

   }

   private static long findGCD(long a, long b){
      long lowest = Math.min(Math.abs(a), Math.abs(b));
      if (a == 0){
         return b;
      }
      else if (b == 1){
         return 1;
      }

      for (long gcd = lowest; gcd > 1; gcd--) {
         if (a % gcd == 0 && b % gcd == 0){
            return gcd;
         }
      }

      return 1;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return num;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */

   public long getDenominator() { 
      return deNom;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      String sNum;
      if(num < 0) {
         sNum = String.format("(%s)", getNumerator());
      }
      else {
         sNum = Long.toString(getNumerator());
      }

      return String.format("%s/%s", sNum, getDenominator());
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m instanceof  Lfraction){
         return compareTo((Lfraction) m) == 0;
      }
      throw new RuntimeException((String.format("Given wrong object %s. Input object has to be instance of Lfraction!",m)));
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Long.hashCode(num) * 2 + Long.hashCode(deNom) * 4;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newDeNom;
      long newNum;

      if (m.deNom == deNom){
         newDeNom =deNom;
         newNum = num + m.num;
      }
      else {
         newDeNom = deNom * m.deNom;
         newNum = num * m.deNom + m.num * deNom;
      }
      return new Lfraction(newNum, newDeNom);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long nNum = m.num * num;
      long nDeNom = m.deNom * deNom;
      return new Lfraction(nNum,nDeNom);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(num == 0){
         throw new RuntimeException((String.format("%s cannot be reversed. Denominator cannot be 0!",(this))));
      }
      else return new Lfraction(deNom, num);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(num * (-1), deNom);
   }

   public Lfraction pow(int n){
      if (n == 0){
         return new Lfraction(1, 1);
      }

      else if (n == 1){
         return new Lfraction(num, deNom);
      }

      else if (n == -1){
         return inverse();
      }

      else if (n > 0){
         return times(pow(n-1));
      }
      else
      {
         return pow(-n).inverse();
      }
   }

   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.num == 0) {
         throw new RuntimeException((String.format("Wrong input %s. Number cannot be divided by zero!",(m))));
      }
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (m.deNom == deNom){
         return Long.compare(num, m.num);
      }
      else {
         return Long.compare(num * m.deNom, m.num * deNom);
      }
   }


   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(num, deNom);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (num / deNom);
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long intPart = integerPart();
      return new Lfraction(num - deNom * intPart, deNom);
   }


   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) num / (double) deNom;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String appropSymblos = "0123456789/(-)";
      for (char c : s.toCharArray()) {
         if (appropSymblos.indexOf(c) == -1){
            throw new RuntimeException((String.format("Wrong input %s. Input contains inappropriate symbols!",(s))));
         }
      }
      String[] operands = s.split("/");
      if (operands.length != 2){
         throw new RuntimeException((String.format("Wrong input %s",(s))));
      }
      long newNum;
      long newDeNom;

      if (operands[0].contains("(")){
         try {
            newNum = Long.parseLong(operands[0].substring(1, operands[0].length() - 1));
         }
         catch (Exception e){
            throw new RuntimeException((String.format("Wrong input %s",(s))));
         }
      }
      else {
         try {
            newNum = Long.parseLong(operands[0]);
         }
         catch (Exception e){
            throw new RuntimeException((String.format("Wrong input %s",(s))));
         }      }

      newDeNom = Long.parseLong(operands[1]);
      return new Lfraction(newNum, newDeNom);
   }
}

